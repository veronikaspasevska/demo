package mk.iwec.demo.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/api")
public class Controller {
	@GetMapping("/message")
	public String showMessage() {
		return "Hello! It works!";
	}
}
